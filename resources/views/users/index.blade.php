<x-master>
    @section('title')
        Users
    @endsection
    @section('card')
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">All Users</h6>
                    <div class="float-right">
                        <a href="{{route('users.create')}}" class="btn btn-success">Create User</a>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    @include('includes.flash')
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" >
                            <thead>
                            <tr>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td></td>

                                    <td>
                                        <form action="{{ route('users.destroy', $user->id) }}" method="POST">

                                            <a href="{{ route('users.edit',$user->id)}}" class="btn btn-warning">Edit</a>

                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    @endsection
    @section('js')
    <!-- Page level plugins -->
        <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

        <!-- Page level custom scripts -->
        <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
    @endsection
</x-master>
