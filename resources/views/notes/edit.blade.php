<x-master>
    @section('title')
        Notes
    @endsection
    @section('card')
        <div class="col-xl-10 col-lg-7">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Edit Notes</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    @include('includes.flash')
                    <form method="POST" action="">
                        @csrf
                        @method('HEAD')
                        <div class="form-group">
                            <label for="note_title">Title</label>
                            <input type="text" class="form-control" id="note_title" name="note_title" value="{{ $note->note_title }}"required>
                            <input type="text" class="form-control" id="user_id" name="user_id" value="{{Auth::user()->id}}" hidden>
                        </div>
                        <div class="form-group">
                            <label for="note_body">Content</label>
                            <textarea name="note_body" id="note_body" cols="30" rows="10" class="form-control">{{ $note->note_body }}"</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Post</button>
                    </form>
                </div>
            </div>
        </div>
    @endsection

</x-master>
