
<x-master>
    @section('title')
        Notes
    @endsection
    @section('card')
        <div class="col-xl-12 col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Edit Notes</h6>
                    <div class="float-right">
                        <a href="{{route('notes.create')}}" class="btn btn-success">Create Note</a>
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    @include('includes.flash')
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" >
                            <thead>
                            <tr>
                                <th>Note Title</th>
                                <th>Note Body</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Note Title</th>
                                <th>Note Body</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($notes as $note)
                                <tr>
                                    <td>{{Str::limit($note->note_title, 25)}}</td>
                                    <td>{{Str::limit($note->note_body,40)}}</td>
                                    <td>
                                        <a href="{{ route('notes.edit',$note->id)}}" class="btn btn-warning">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    @endsection
    @section('js')
    <!-- Page level plugins -->
        <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

        <!-- Page level custom scripts -->
        <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
    @endsection
</x-master>
