<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    protected $fillable = ['note_title', 'note_body', 'tags','user_id'];


    public function user(){
        return $this->belongsTo(User::class);
    }

}
