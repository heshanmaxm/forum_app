<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->user_type == 'admin'){

            $notes = Note::all();
            return view('notes.index',compact('notes'));

        }
        else{

            $notes = Note::where('user_id',Auth::user()->id)->get();
            return view('notes.index',compact('notes'));

        }
    }
}
