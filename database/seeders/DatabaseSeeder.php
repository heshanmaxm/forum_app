<?php

namespace Database\Seeders;

use App\Models\Note;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->count(10)
            ->has(Note::factory()->count(2), 'notes')
            ->create();

        $note = Note::factory()
            ->has(Tag::factory()->count(2), 'tags')
            ->create();
    }
}
